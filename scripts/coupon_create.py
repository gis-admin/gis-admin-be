import random
import string

AMOUNT = {
    "ten": 500,
    "twenty": 500,
    "thirty": 100,
    "free": 50,
}

CODE_CHOICE = string.digits + string.ascii_uppercase
CODE_LEN = 9

for _key in AMOUNT.keys():
    code_list = list()
    with open(f"../code/{_key}.txt", "r") as f:
        for line in f:
            code_list.append(line.strip())
    
    while len(code_list) < AMOUNT[_key]:
        code = "".join(random.choice(CODE_CHOICE) for _ in range(CODE_LEN))
    
        if code in code_list: continue
        code_list.append(code)

    with open(f"../code/{_key}.txt", "w") as f:
        for code in code_list:
            f.write(f"{code}\n")

