from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    is_phase_one = models.BooleanField(
        default=False,
        help_text="whether the user is only test on phase one",
    )

    class Meta:
        db_table = "user"
