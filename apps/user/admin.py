from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User

# Register your models here.


class UserAdminCustom(UserAdmin):
    model = User
    list_display = ("username", "is_superuser", "is_phase_one")
    fieldsets = (
        (None, {"fields": ("username", "password", "email")}),
        ("Permissions", {"fields": ("is_staff", "is_phase_one", "is_superuser")}),
    )


admin.site.register(User, UserAdminCustom)
