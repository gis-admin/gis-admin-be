from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated, IsAdminUser, AllowAny
from . import models, serializers, permissions
from rest_framework.pagination import LimitOffsetPagination
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError


class DelegateViewSet(viewsets.ModelViewSet):
    queryset = models.Delegate.objects.all()
    serializer_class = serializers.DelegateSerializer
    pagination_class = LimitOffsetPagination
    filter_backends = [DjangoFilterBackend, OrderingFilter]

    filterset_fields = {
        "interview_time": ["gte", "lte", "exact"],
    }

    search_fields = [
        "@name",
        "@giscode",
    ]

    ordering_fields = ["create_time"]

    def get_queryset(self):
        return models.Delegate.objects.all()

    def get_permissions(self):
        if self.action in ["create", "multi_create"]:
            permission_classes = [
                AllowAny,
            ]
        elif self.action in ["update", "delete", "partial_update"]:
            permission_classes = [
                (IsAuthenticated & (permissions.CanGradeEssay | IsAdminUser))
            ]
        else:
            permission_classes = [IsAuthenticated]

        return [permission() for permission in permission_classes]

    @action(
        detail=False,
        methods=["post"],
        url_path="multi",
        url_name="multi",
    )
    def multi_create(self, request, *args, **kwargs):
        serializer = serializers.MultiDelegateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        if (
            len(serializer.validated_data["delegate"]) != 3
            and len(serializer.validated_data["delegate"]) != 5
        ):
            raise ValidationError("Invalid Length of delegate")

        group = models.Group.objects.create()
        group.save()

        for delegate in serializer.validated_data["delegate"]:
            d = models.Delegate.objects.create(
                **delegate,
            )
            d.group = group
            d.save()

            # serializer = serializers.SingleDelegateSerializer(
            #     **delegate,
            # )
            # if delegate.is_valid():
            #     ret = delegate.save()
            #     ret.group = group
            #     ret.save()
            # else:
            #     print("Error: ", delegate.errors)
            #     raise ValidationError("The data is invalid")

        return Response(
            "Success", status=status.HTTP_201_CREATED
        )
