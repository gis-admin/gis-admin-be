from django.test import TestCase
from .models import Delegate
from datetime import date
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from apps.grade.models import Grade
from django.urls import reverse


class DelegateModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Delegate.objects.create(
            gis_code="GIS20000",
            name="testing_name",
            passport_name="Wang testing",
            education_level="bachelor",
            current_institution="NTU",
            major="Information Management",
            sex="male",
            nationality="Taiwan",
            year="sophomore",
            interview_time=date.today(),
        )

    def test_object_name_is_correct(self):
        delegate = Delegate.objects.get(id=1)
        expected_object_name = "testing_name"

        self.assertEqual(str(delegate), expected_object_name)


class DelegateSerializerandViewTest(APITestCase):
    @classmethod
    def setUpTestData(cls):
        delegate = Delegate.objects.create(
            gis_code="GIS20000",
            name="testing_name",
            passport_name="Wang testing",
            education_level="bachelor",
            current_institution="NTU",
            major="Information Management",
            sex="male",
            nationality="Taiwan",
            year="sophomore",
            interview_time=date.today(),
        )
        delegate_2 = Delegate.objects.create(
            gis_code="GIS20002",
            name="testing_name_2",
            passport_name="Wang testing",
            education_level="bachelor",
            current_institution="NTU",
            major="Information Management",
            sex="male",
            nationality="Taiwan",
            year="sophomore",
            interview_time=date.today(),
        )
        user_1 = get_user_model().objects.create(
            username="dodofk",
            password=make_password("password"),
            is_phase_one=True,
        )
        user_2 = get_user_model().objects.create(
            username="dodofk2",
            password=make_password("password"),
        )
        get_user_model().objects.create_superuser(
            username="superuser",
            password="password",
        )
        Grade.objects.create(
            user=user_1,
            delegate=delegate,
            self_introduction=10.5,
            behavior=12.5,
            academic=17.5,
            challenging=7.5,
            overall=12.5,
            is_completed=True,
        )
        Grade.objects.create(
            user=user_2,
            delegate=delegate_2,
            is_completed=False,
        )

    def get_user_token(self, username, password):
        url = reverse("jwt-create")
        data = {
            "username": username,
            "password": password,
        }
        response = self.client.post(url, data, format="json")
        return f"Bearer {response.data['access']}"

    def test_get_delegate_with_permission_and_grade_and_status(self):
        token = self.get_user_token(username="dodofk", password="password")

        url = reverse("delegate-list")

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=token)
        response = client.get(
            url,
        )

        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["gis_code"], "GIS20000")
        self.assertEqual(response.data["results"][0]["grade"], 302.5)
        self.assertEqual(response.data["results"][0]["status"], True)

    def test_get_delegate_with_grade_not_complete(self):
        token = self.get_user_token(username="dodofk2", password="password")

        url = reverse("delegate-list")

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=token)
        response = client.get(
            url,
        )

        self.assertEqual(response.data["results"][0]["gis_code"], "GIS20002")
        self.assertEqual(response.data["results"][0]["grade"], None)
        self.assertEqual(response.data["results"][0]["status"], False)

    def test_update_permissions_with_phase_one_user(self):
        id = Delegate.objects.get(gis_code="GIS20000").id
        url = reverse("delegate-detail", args=(id,))
        data = {
            "essay_grade": 12.5,
        }

        client = APIClient()
        client.credentials(
            HTTP_AUTHORIZATION=self.get_user_token(
                username="dodofk", password="password"
            )
        )

        response = client.patch(
            url,
            data,
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["essay_grade"], 12.5)
