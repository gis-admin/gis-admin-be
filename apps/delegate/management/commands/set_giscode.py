from django.core.management.base import BaseCommand
from apps.delegate.models import Delegate


class Command(BaseCommand):
    help = "Set gis code to existing delegate"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        queryset = Delegate.objects.order_by("create_time")
        print(len(queryset))
        for delegate in queryset:
            if delegate.gis_code:
                delegate.gis_code = ""
                delegate.save()
        # exist_code = list()
        # for delegate in queryset:
        #     if delegate.gis_code:
        #         exist_code.append(int(delegate.gis_code[3:]))
        # print(exist_code)
        # min_num = min(1, min(exist_code)) if exist_code else 1

        # for delegate in queryset:

        #     if delegate.gis_code:
        #         min_num += 1
        #         continue
        #     if min_num in exist_code:
        #         min_num += 1
        #         continue

        #     delegate.gis_code = f"G23{str(min_num).zfill(3)}"
        #     # print(delegate.gis_code)
        #     delegate.save()
        #     min_num += 1