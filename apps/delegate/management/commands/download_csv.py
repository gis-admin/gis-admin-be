from django.core.management.base import BaseCommand
from apps.delegate.models import Delegate
import csv

FIELDS_TO_CSV = [
    "gis_code",
    "first_name",
    "last_name",
    "prefer_name",
    "term_condition",
    "date_of_birth",
    "sex",
    "prefer_gender",
    "nationality",
    "nationality_2",
    "education_institution",
    "education_level",
    "major",
    "dietary_requirement",
    "email",
    "phone_number",
    "mail_address",
    "facebook_link",
    "emergency_contact_name",
    "emergency_phone_number",
    "first_choice",
    "second_choice",
    "third_choice",
    "fourth_choice",
    "panel_discussion",
    "create_time",
    "essay_grade_1",
    "essay_grade_2",
    "self_introduction",
    "behavior",
    "academic",
    "challenging",
    "overall"
]


class Command(BaseCommand):
    help = "download delegate to csv"

    def add_arguments(self, parser):
        parser.add_argument("--path", type=str, help="file path to save csv")

    def handle(self, *args, **options):
        queryset = Delegate.objects.order_by("create_time")
        print(len(queryset))
        with open(options["path"], "w") as file:
            writer = csv.writer(file)
            writer.writerow(FIELDS_TO_CSV)

            for delegate in queryset:
                delegate_fields = list()

                for field in FIELDS_TO_CSV:
                    delegate_fields.append(getattr(delegate, field))
                writer.writerow(delegate_fields)
