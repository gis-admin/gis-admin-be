from django.core.management.base import BaseCommand
from apps.delegate.models import Coupon

CODE_TYPE = ["ten", "twenty", "thirty", "free"]


class Command(BaseCommand):
    help = "add predefined code to db"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        for _key in CODE_TYPE:
            with open(f"code/{_key}.txt", "r") as f:
                code_list = f.read().split("\n")[:-1]
                for code in code_list:
                    if Coupon.objects.filter(code=code).exists():
                        continue
                    coupon = Coupon.objects.create(
                        code=code,
                        discount=_key,
                    )
                    coupon.save()
