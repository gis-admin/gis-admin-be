from django.db import models
from gis_admin_be.storage_backends import PrivateMediaStorage
import uuid


class Group(models.Model):
    class Meta:
        db_table = "group"


def delegate_file_path(instance, filename):
    u = str(uuid.uuid4())[0:6]
    return f"delegate_{instance.first_name}_{instance.last_name}_{u}/{filename}"




class CouponReusable(models.Model):
    code = models.CharField(
        unique=True,
        null=False,
        primary_key=True,
        max_length=20,
        help_text="code"
    )

    class Discount(models.TextChoices):
        ten = "ten", "ten percent off"
        twenty = "twenty", "twenty percent off"
        thirty = "thirty", "thirty percent off"
        free = "free", "free of charge"

    discount = models.CharField(
        max_length=20,
        choices=Discount.choices,
        default=Discount.ten,
    )
    limit = models.IntegerField(
        default=30,
        help_text="max user to use discount",
    )

    class Meta:
        db_table = "coupon_reusable"

    def __str__(self):
        return f"Code {self.code}"


class Delegate(models.Model):
    group = models.ForeignKey(
        Group,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    gis_code = models.CharField(
        max_length=30,
        unique=True,
        help_text="gis code for each user",
        blank=True,
        null=True,
    )
    term_condition = models.BooleanField(
        default=False,
        help_text="whether the delegate agree term and condition",
    )
    first_name = models.CharField(
        default="",
        null=False,
        max_length=100,
        help_text="name of delegate",
    )
    last_name = models.CharField(
        default="",
        null=False,
        max_length=100,
        help_text="name of delegate",
    )
    prefer_name = models.CharField(
        null=True,
        max_length=100,
        help_text="prefer name of delegate",
    )
    date_of_birth = models.DateField(
        null=True,
        blank=True,
        help_text="birth date of delegate",
    )
    sex = models.CharField(
        default="",
        max_length=100,
        help_text="sex of delegate",
    )
    prefer_gender = models.CharField(
        default="",
        max_length=100,
        help_text="prefer gender pronoun of delegate",
    )
    nationality = models.CharField(
        default="",
        max_length=100,
        help_text="nationality of delegate",
    )
    nationality_2 = models.CharField(
        default="",
        max_length=100,
        help_text="second nationality of delegate",
        blank=True,
    )
    education_institution = models.CharField(
        default="",
        max_length=100,
        help_text="current institution of the delegate",
    )
    education_level = models.CharField(
        default="",
        max_length=100,
        help_text="education level of the delegate",
    )
    major = models.CharField(
        default="",
        max_length=100,
        help_text="major of the delegate",
    )
    dietary_requirement = models.CharField(
        default="",
        max_length=100,
        help_text="dietary requirement of delegate",
    )
    email = models.EmailField(
        null=True,
        help_text="email of delegate",
    )
    phone_number = models.CharField(
        default="",
        max_length=100,
        help_text="phone number of delegate",
        null=True,
    )
    mail_address = models.CharField(
        default="",
        max_length=200,
        help_text="mail address of delegate",
        null=True,
    )
    facebook_link = models.CharField(
        max_length=200,
        help_text="facebook link of delegate",
        default=True,
    )
    emergency_contact_name = models.CharField(
        default="",
        max_length=150,
        help_text="emergency contact name of delegate",
    )
    emergency_phone_number = models.CharField(
        default="",
        max_length=150,
        help_text="emergency contact phone number of delegate",
    )
    emergency_relation = models.CharField(
        default="",
        max_length=250,
        help_text="relationship with emergency contact of delegate",
    )
    first_choice = models.CharField(
        default="",
        max_length=100,
        help_text="first choice of academic topic",
    )
    second_choice = models.CharField(
        default="",
        max_length=100,
        help_text="second choice of academic topic",
    )
    third_choice = models.CharField(
        default="",
        max_length=100,
        help_text="third choice of academic topic",
    )
    fourth_choice = models.CharField(
        default="",
        max_length=100,
        help_text="fourth choice of academic topic",
    )
    panel_discussion = models.BooleanField(
        help_text="i dnk",
    )
    essay_assignment = models.FileField(
        upload_to=delegate_file_path,
        help_text="file of essay",
        storage=PrivateMediaStorage,
        blank=True,
        null=True,
    )
    interview_time = models.DateField(
        null=True,
        blank=True,
        help_text="when the delegate need to be interviewed",
    )
    essay_grade_1 = models.FloatField(
        null=True,
        blank=True,
        help_text="essay grade one of the delegate",
    )
    essay_grade_2 = models.FloatField(
        null=True,
        blank=True,
        help_text="essay grade two of the delegate",
    )
    create_time = models.DateTimeField(
        auto_now_add=True,
        blank=True,
        null=True,
    )
    self_introduction = models.FloatField(
        null=True,
        blank=True,
        help_text="grade of self introduction",
    )
    behavior = models.FloatField(
        null=True,
        blank=True,
        help_text="grade of behavior",
    )
    academic = models.FloatField(
        null=True,
        blank=True,
        help_text="grade of academic",
    )
    challenging = models.FloatField(
        null=True,
        blank=True,
        help_text="grade of challenging",
    )
    overall = models.FloatField(
        null=True,
        blank=True,
        help_text="Overall grade",
    )
    coupon_reusable = models.ForeignKey(
        CouponReusable,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="delegate",
    )

    def __str__(self):
        return f"{self.last_name} {self.first_name}"

    class Meta:
        db_table = "delegate"


class Coupon(models.Model):
    code = models.CharField(
        unique=True,
        null=False,
        primary_key=True,
        max_length=20,
        help_text="code for the coupon",
    )

    class Discount(models.TextChoices):
        ten = "ten", "ten percent off"
        twenty = "twenty", "twenty percent off"
        thirty = "thirty", "thirty percent off"
        free = "free", "free of charge"

    discount = models.CharField(
        max_length=20,
        choices=Discount.choices,
        default=Discount.ten,
    )
    delegate = models.OneToOneField(
        Delegate,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="delegate",
    )

    class Meta:
        db_table = "coupon"

    def __str__(self):
        return f"Code {self.code}"




