from django.urls import include, path
from . import views
from rest_framework_nested.routers import DefaultRouter

router = DefaultRouter()
router.register(r"delegate", views.DelegateViewSet, basename="delegate")

urlpatterns = [
    path("", include(router.urls)),
]
