from rest_framework.permissions import BasePermission


class CanGradeEssay(BasePermission):
    def has_object_permission(self, request, view, obj):
        return request.user.is_phase_one
