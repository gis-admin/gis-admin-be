import io
import PyPDF2
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from drf_extra_fields.fields import Base64FileField
from drf_base64.serializers import ModelSerializer
from . import models


class DelegateSerializer(serializers.ModelSerializer):
    code = serializers.CharField(
        max_length=100,
        allow_blank=True,
        required=False,
    )

    class Meta:
        model = models.Delegate
        fields = [
            "code",
            "term_condition",
            "first_name",
            "last_name",
            "prefer_name",
            "date_of_birth",
            "sex",
            "prefer_gender",
            "nationality",
            "nationality_2",
            "education_institution",
            "education_level",
            "major",
            "dietary_requirement",
            "email",
            "phone_number",
            "mail_address",
            "facebook_link",
            "emergency_contact_name",
            "emergency_phone_number",
            "emergency_relation",
            "first_choice",
            "second_choice",
            "third_choice",
            "fourth_choice",
            "panel_discussion",
            "essay_assignment",
            "essay_grade_1",
            "essay_grade_2",
            "self_introduction",
            "behavior",
            "academic",
            "challenging",
            "overall",
        ]

    def create(self, validated_data):
        if "code" not in validated_data:
            delegate = models.Delegate.objects.create(
                **validated_data,
            )
            delegate.save()
            return delegate

        if models.CouponReusable.objects.filter(code=validated_data["code"]).exists():
            coupon_reusable = models.CouponReusable.objects.get(code=validated_data["code"])
            if coupon_reusable.delegate.count() >= coupon_reusable.limit:
                raise ValidationError("the coupon has been used up")
            validated_data.pop("code")
            delegate = models.Delegate.objects.create(
                coupon_reusable=coupon_reusable,
                **validated_data,
            )
            delegate.save()
            return delegate

        elif models.Coupon.objects.filter(code=validated_data["code"]).exists():
            coupon = models.Coupon.objects.get(code=validated_data["code"])
            if coupon.delegate is not None:
                raise ValidationError("the coupon has used")

            validated_data.pop("code")
            delegate = models.Delegate.objects.create(
                **validated_data,
            )
            delegate.save()

            coupon.delegate = delegate
            coupon.save()

            return delegate
        else:
            raise ValidationError("the coupon is not valid")

    def to_representation(self, instance: models.Delegate):
        return {
            "gis_code": instance.gis_code,
            "id": instance.id,
            "term_condition": instance.term_condition,
            "first_name": instance.first_name,
            "last_name": instance.last_name,
            "prefer_name": instance.prefer_name,
            "date_of_birth": instance.date_of_birth,
            "sex": instance.sex,
            "prefer_gender": instance.prefer_gender,
            "nationality": instance.nationality,
            "nationality_2": instance.nationality_2,
            "education_institution": instance.education_institution,
            "education_level": instance.education_level,
            "major": instance.major,
            "dietary_requirement": instance.dietary_requirement,
            "email": instance.email,
            "phone_number": instance.phone_number,
            "mail_address": instance.mail_address,
            "facebook_link": instance.facebook_link,
            "emergency_contact_name": instance.emergency_contact_name,
            "emergency_phone_number": instance.emergency_phone_number,
            "first_choice": instance.first_choice,
            "second_choice": instance.second_choice,
            "third_choice": instance.third_choice,
            "fourth_choice": instance.fourth_choice,
            "panel_discussion": instance.panel_discussion,
            "essay_assignment": instance.essay_assignment.url if instance.essay_assignment else None,
            "create_time": instance.create_time,
            "essay_grade_1": instance.essay_grade_1,
            "essay_grade_2": instance.essay_grade_2,
            "self_introduction": instance.self_introduction,
            "behavior": instance.behavior,
            "academic": instance.academic,
            "challenging": instance.challenging,
            "overall": instance.overall,
        }


class PDFBase64File(Base64FileField):
    ALLOWED_TYPES = ['pdf']

    def get_file_extension(self, filename, decoded_file):
        try:
            PyPDF2.PdfFileReader(io.BytesIO(decoded_file))
        except Exception as e:
            print(e)
        else:
            return 'pdf'


class SingleDelegateSerializer(ModelSerializer):
    essay_assignment = PDFBase64File()

    class Meta:
        model = models.Delegate
        fields = [
            "term_condition",
            "first_name",
            "last_name",
            "prefer_name",
            "date_of_birth",
            "sex",
            "prefer_gender",
            "nationality",
            "nationality_2",
            "education_institution",
            "education_level",
            "major",
            "dietary_requirement",
            "email",
            "phone_number",
            "mail_address",
            "facebook_link",
            "emergency_contact_name",
            "emergency_phone_number",
            "emergency_relation",
            "first_choice",
            "second_choice",
            "third_choice",
            "fourth_choice",
            "panel_discussion",
            "essay_assignment",
        ]


class MultiDelegateSerializer(serializers.Serializer):
    delegate = SingleDelegateSerializer(many=True)

    class Meta:
        fields = [
            "delegate",
        ]

    def create(self, validated_data):
        if len(validated_data["delegate"]) != 3 and len(validated_data["delegate"]) != 5:
            print(len(validated_data["delegate"]))
            raise ValidationError("Invalid Length of delegate")

        group = models.Group.objects.create()
        group.save()

        for delegate in validated_data["delegate"]:
            delegate = models.Delegate.objects.create(
                **delegate,
            )
            delegate.group = group
            delegate.save()

