from django.test import TestCase
from apps.delegate.models import Delegate
from .models import Priority
from datetime import date


class PriorityModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        delegate = Delegate.objects.create(
            gis_code="GIS20000",
            name="testing_name",
            passport_name="Wang testing",
            education_level="bachelor",
            current_institution="NTU",
            major="Information Management",
            sex="male",
            nationality="Taiwan",
            year="sophomore",
            interview_time=date.today(),
        )
        Priority.objects.create(
            delegate=delegate,
            ap_priority_1="1",
            ap_priority_2="2",
            ap_priority_3="3",
            ap_priority_4="4",
            speech_priority_1="1",
            speech_priority_2="2",
        )

    def test_object_name_is_correct(self):
        priority = Priority.objects.get(id=1)
        expected_object_name = f"Priority of testing_name"

        self.assertEqual(str(priority), expected_object_name)