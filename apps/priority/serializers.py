from rest_framework import serializers
from . import models


class PrioritySerializer(serializers.HyperlinkedModelSerializer):
    delegate = serializers.PrimaryKeyRelatedField(
        many=False,
        read_only=True,
    )

    class Meta:
        model = models.Priority
        fields = [
            "id",
            "url",
            "delegate",
            "ap_priority_1",
            "ap_priority_2",
            "ap_priority_3",
            "ap_priority_4",
            "speech_priority_1",
            "speech_priority_2",
        ]
