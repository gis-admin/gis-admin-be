from django.urls import include, path
from . import views
from rest_framework_nested.routers import DefaultRouter

router = DefaultRouter()
router.register(r"priority", views.PriorityViewSet, basename="priority")

urlpatterns = [
    path("", include(router.urls)),
]
