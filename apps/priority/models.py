from django.db import models


# todo: write in a stupid way for convenience
class Priority(models.Model):
    delegate = models.OneToOneField(
        "delegate.Delegate",
        on_delete=models.CASCADE,
        related_name="priority",
        help_text="corresponding priority to the delegate",
    )
    ap_priority_1 = models.CharField(
        max_length=100,
        help_text="AP Priority 1 of the delegate",
    )
    ap_priority_2 = models.CharField(
        max_length=100,
        help_text="AP Priority 2 of the delegate",
    )
    ap_priority_3 = models.CharField(
        max_length=100,
        help_text="AP Priority 3 of the delegate",
    )
    ap_priority_4 = models.CharField(
        max_length=100,
        help_text="AP Priority 4 of the delegate",
    )
    speech_priority_1 = models.CharField(
        max_length=100,
        help_text="Speech Priority 1 of the delegate",
    )
    speech_priority_2 = models.CharField(
        max_length=100,
        help_text="Speech Priority 1 of the delegate",
    )

    class Meta:
        db_table = "priority"
        verbose_name_plural = "priorities"

    # def __str__(self):
    #     return f"Priority of {self.delegate.name}"
