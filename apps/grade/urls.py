from django.urls import include, path
from . import views
from rest_framework_nested.routers import DefaultRouter

router = DefaultRouter()
router.register(r"grade", views.GradeViewSet, basename="grade")

urlpatterns = [
    path("", include(router.urls)),
]
