from rest_framework import serializers
from . import models


class GradeSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.PrimaryKeyRelatedField(
        many=False,
        read_only=True,
    )
    delegate = serializers.PrimaryKeyRelatedField(
        many=False,
        read_only=True,
    )
    total_score = serializers.SerializerMethodField()

    class Meta:
        model = models.Grade
        fields = [
            "id",
            "url",
            "user",
            "delegate",
            "self_introduction",
            "behavior",
            "academic",
            "challenging",
            "overall",
            "is_completed",
            "total_score",
        ]

    def get_total_score(self, obj: models.Grade):
        if (
            obj.is_completed
            and obj.self_introduction is not None
            and obj.behavior is not None
            and obj.academic is not None
            and obj.challenging is not None
            and obj.overall is not None
        ):
            return (
                obj.self_introduction
                + obj.behavior
                + obj.academic
                + obj.challenging
                + obj.overall
            ) * 5
        else:
            return None
