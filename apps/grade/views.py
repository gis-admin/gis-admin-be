from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.pagination import LimitOffsetPagination
from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import extend_schema_view, extend_schema

from . import models, serializers, permissions


@extend_schema_view(
    list=extend_schema(
        description="use this endpoint and add delegate id to filter, if the user have grade "
        "on the delegate then it will return one instance, otherwise, it will return empty",
    ),
    retrieve=extend_schema(
        description="if the user is not the user who need to grade, it will have no permission to get it",
    ),
)
class GradeViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.GradeSerializer
    pagination_class = LimitOffsetPagination
    filter_backends = [DjangoFilterBackend]
    filterset_fields = {
        "delegate": ["exact"],
    }

    def get_queryset(self):
        return models.Grade.objects.filter(user=self.request.user)

    def get_permissions(self):
        if self.action in ["retrieve", "update"]:
            permission_classes = [IsAuthenticated, permissions.IsGradeUser]
        else:
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]
