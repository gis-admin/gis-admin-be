from django.db import models
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError


def restrict_grade_amount(index):
    print("testing", Grade.objects.filter(delegate_id=index))
    if Grade.objects.filter(delegate_id=index).count() == 2:
        raise ValidationError("Only two user can grade one delegate")


class Grade(models.Model):
    user = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        related_name="grade",
        help_text="user who make the grade",
    )
    delegate = models.ForeignKey(
        "delegate.Delegate",
        on_delete=models.CASCADE,
        related_name="grade",
        help_text="delegate who be graded",
        validators=(restrict_grade_amount,),
    )
    self_introduction = models.FloatField(
        null=True,
        blank=True,
        help_text="grade of self introduction",
    )
    behavior = models.FloatField(
        null=True,
        blank=True,
        help_text="grade of behavior",
    )
    academic = models.FloatField(
        null=True,
        blank=True,
        help_text="grade of academic",
    )
    challenging = models.FloatField(
        blank=True,
        null=True,
        help_text="grade of chaalenging?",
    )
    overall = models.FloatField(
        blank=True,
        null=True,
        help_text="grade of overall performance",
    )
    is_completed = models.BooleanField(
        default=False,
        help_text="whether the grade is finish",
    )

    def __str__(self):
        return f"grade of {self.delegate.name} by {self.user.username}"

    class Meta:
        db_table = "grade"
